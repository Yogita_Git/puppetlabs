class myclass{
file {'/root/devops.txt':                                            # resource type file and filename
                ensure  => present,                                               # make sure it exists
                 mode    => '0644',                                                # file permissions
                 content => "This is a devops class.",                                         # note the ipaddress_eth0 fact
                       }
}
